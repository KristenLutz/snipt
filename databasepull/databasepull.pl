#!/usr/bin/perl
use strict;
use warnings;
use LWP; 
use LWP::Simple;
use DBI;

#connect to database
#my $dbh =DBI->connect("null", 'null', 'null') or die("Problem connecting: ");

sub trim {
    (my $s = $_[0]) =~ s/^\s+|\s+$//g;
    return $s;        
}

sub getTagValues{
    #(str:tag, str:s)->array of str
    # Return values encapsulated by tag in s
    my $tag = $_[0];
    my $s = $_[1];
    my @values;
    while ($s=~m#<$tag.*?>(.*?)</$tag>#gs) {
        push @values, trim($1);
    }
    return(@values);
}

sub readPsicquic {
    
    #reads database name service url, query protien, and number of entries limit
    my ($db, $url, $query, $size ) = @_;
    
    #construct query url
    my $queryUrl = $url . "/current/search/query/" . $query . "?firstResult=0&maxResults=" . $size;
    
    #get results returned from query
    my $content = get $queryUrl;
    die "Couldn't get $queryUrl" unless defined $content;

    #list of all interactions
    my @lines = split(/\n/, $content);
    
    OUTER:foreach my $line (@lines) {
        
        #split tab delimited lines  
        my @flds = split(/\t/, $line);
           
        #read array of feilds into variables
        my ($idA, $idB, $altIdA, $altIdB, $aliasA, $aliasB, $detMethod, $author, $pub1, $orgA, $orgB, $intType, $sourceDb1, $intId, $conf) = @flds;
        
        #define variables used bellow
        my @aA;
        my @aB;
        my $newAliasA;
        my $newAliasB;
        my $newOrgA;
        my $newOrgB;
        my $idAh;
        my $idBh;
        my $date;
        my $fullNameA;
        my $fullNameB;
        
        #get the pubmed id of article presenting interaction
        $pub1 =~ /pubmed:([0-9]*)/;
        my $pub = $1;
        #print $pub1;
        #if wrong value returned for pubmed id look in $line for correct id
        if ($pub =~ /[0-9]{8}/) {
        } else {
            $line =~ /.*?([0-9]{8})/;
            $pub = $1;
            next OUTER unless defined $pub;
        }
        
        #construct the url to access the article associated with the pubmed id  
        my $pubUrl = "http://www.ncbi.nlm.nih.gov/pubmed/" . $pub;

        #clean up names and taxid ids for entries from STRING  
        if ($url eq "http://string.uzh.ch/psicquic/webservices") {
            $aliasA =~ /string:(.*)/;
            $newAliasA = $1;
            $aliasB =~ /string:(.*)/;
            $newAliasB = $1;
            $orgA =~ /taxid:([0-9]*)/;
            $newOrgA = $1;
            $orgB =~ /taxid:([0-9]*)/;
            $newOrgB = $1;
        
        #clean up names and taxid ids for entries from IRefIndex    
        } elsif($url eq "http://psicquic.irefindex.org/psicquic/webservices") {
    
            $aliasA =~ /^hgnc:(.*?)\|/;
            $newAliasA = $1;
            $aliasB =~ /^hgnc:(.*?)\|/;
            $newAliasB = $1;
            $orgA =~ /^taxid:([0-9]*)/;
            $newOrgA = $1;
            $orgB =~ /^taxid:([0-9]*)/;
            $newOrgB = $1;
            
        #clean up names and taxid ids for entries form IntAct
        } elsif($url eq "http://www.ebi.ac.uk/Tools/webservices/psicquic/intact/webservices") {
    
            $aliasA =~ /\|uniprotkb:(.*?)\(gene name\)\|/;
            $newAliasA = $1;
            $aliasB =~ /\|uniprotkb:(.*?)\(gene name\)\|/;
            $newAliasB = $1;
            $orgA =~ /^taxid:([0-9]*)/;
            $newOrgA = $1;
            $orgB =~ /^taxid:([0-9]*)/;
            $newOrgB = $1;
            
        #clean up names and taxid ids for entries from BioGrid 
        } elsif($url eq "http://tyersrest.tyerslab.com:8805/psicquic/webservices") {
    
            $altIdA =~ /entrez gene\/locuslink:([A-Za-z0-9]*)/;
            $newAliasA = $1;
            $altIdB =~ /entrez gene\/locuslink:([A-Za-z0-9]*)/;
            $newAliasB = $1;
            $orgA =~ /^taxid:([0-9]*)/;
            $newOrgA = $1;
            $orgB =~ /^taxid:([0-9]*)/;
            $newOrgB = $1;
        }
        
        #for interactions in human
        if ($newOrgA == 9606 && $newOrgB == 9606) {
            
            #search NCBI for Protien A    
            my $Acontent = get "http://www.ncbi.nlm.nih.gov/gene/?term=$newAliasA+AND+human";
            
            #find gene ID for ProtienA
            $Acontent =~ /<div class="ui-ncbigrid-outer-div">.*?<span class="gene-id">ID: ([0-9]*)<\/span><\/td><td>(.*?)\[/;
            
            $idAh = $1;
            $fullNameA = $2;
            if ($fullNameA =~ /<span class="highlight" style="background-color:">/) {
                $fullNameA =~ /<span class="highlight" style="background-color:">(.*?)<\/span>/;
                $fullNameA = $1;
            }
            

            #search NCBI for Protien B
            my $Bcontent = get "http://www.ncbi.nlm.nih.gov/gene/?term=$newAliasB+AND+human";
            
            #find gene ID for ProtienB
            $Bcontent =~ /<div class="ui-ncbigrid-outer-div">.*?<span class="gene-id">ID: ([0-9]*?)<\/span><\/td><td>(.*?)\[/;
            
            $idBh = $1;
            $fullNameB = $2;
            if ($fullNameB =~ /<span class="highlight" style="background-color:">/) {
                $fullNameB =~ /<span class="highlight" style="background-color:">.*?<\/span>(.*?)\[/;
                $fullNameB = $1;
            }
            #get date of publication
            
            my $pubcontent = get "$pubUrl";
            open my $errorlog, ">>", "errorlog.txt";
            print $errorlog "$pubUrl\n" unless defined $pubcontent;
            $pubcontent =~ /<div class="rprt_all">.*?<\/a> ([0-9]{4})/;
            $date = $1;
            
            if (defined $idAh && defined $idBh && defined $pubUrl && defined $date && defined $db&& defined $newAliasA && defined $newAliasB && defined $fullNameA && defined $fullNameB) {
               
            
                #my $in1 = "INSERT INTO protein(p_id, p_sym, p_fullname, p_of_interest)VALUES (?, ?, ?, ?);";
                #my $sth = $dbh->prepare($in1) or die "Problem with prepare: " . DBI->errstr;
                #my $insert1a = $sth->execute($idAh, $newAliasA, $fullNameA, "0");# or die "Problem with execute: " . DBI->errstr;
                #my $insert1b = $sth->execute($idBh, $newAliasB, $fullNameB, "0");# or die "Problem with execute: " . DBI->errstr;
                #
                #
                ##insert gene ids pubmed url publication date into database
                #my $in2 = "INSERT INTO ppi(pA_id, pB_id, url, date_published, source_name, annotated)VALUES (?, ?, ?, ?, ?, ?);";
                #my $sth2 = $dbh->prepare($in2) or die "Problem with prepare: " . DBI->errstr;
                #my $insert2 = $sth2->execute($idAh, $idBh, $pubUrl, $date, $db, "Yes");# or die "Problem with execute: " . DBI->errstr;
                
                #creat csv log file
                open my $outfile, ">>", "databaselog4.csv";
                print $outfile "$newAliasA, $fullNameA, $idAh, $newOrgA, $fullNameB, $newAliasB, $newOrgB, $idBh, $pub, $pubUrl, $date, $db\n";
                close $outfile;
                
                #open my $outfile2, ">>", "databaselog3.csv";
                #print $outfile2 "$idAh, $newAliasA, $fullNameA, $idBh, $newAliasB, $fullNameB\n";
                #close $outfile2;
                #
                #print ", $idAh, $newaliasA, $idBh, $newaliasB, $puburl, $date\n";
                #print "idA: $idA | idB: $idB | altIdA: $altIdA | altIdB: $altIdB" . "\n" . "aliasA: $aliasA | aliasB: $aliasB | detMethod: $detMethod" . "\n" . "author: $author | pub: $pub1 | orgA: $orgA | orgB: $orgB" ."\n" . "intType: $intType | sourceDb1: $sourceDb1 | intId: $intId | conf: $conf" . "\n\n\n";
            } else {
                print $errorlog "$newAliasA, $fullNameA, $idAh, $fullNameB, $newAliasB, $idBh, $pub, $pubUrl, $date, $db\n";
            }
        }
    }
}

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$#

#Get html from website and provide error message if cannot or format is wrong
my $browser = LWP::UserAgent->new;

my @alias;

foreach my $j (@ARGV) {
    

my $gene = "$j";

my $url = "http://www.ncbi.nlm.nih.gov/gene/?term=$gene";
my $response = $browser->get( $url );

die "Can't get $url -- ", $response->status_line unless $response->is_success;
die "Expect HTML, not ", $response->content_type unless $response->content_type eq 'text/html';

#Get table content from html code w/o the headers
my $content = $response->decoded_content;
my $sym="";
my $fullname="";
my $entrez_id="";
my $species = "human";
$, = ',';
my $tag_c = "span";
my $is_found = 0;

#Check to see if website is tabular or summarydoc or other
if ($content =~ /<meta name=\"ncbi_report\".*?content=\"tabular\".*?\/>/s){
    #Get table data
    $content =~ /(<table.*?<\/table>)/s;
    my $table = $1;
    #Evaluate table data by row
    while ($table=~/<tr(.*?)<\/tr>/gs && not $is_found) {
        my $row = $1;
        #print "$1";
        my $uc_gene = uc $gene;
        #my $lc_gene = lc $gene;
        my $uc_row = uc $row; #use to compare uc input gene with uc gene in row
        if ($uc_row=~/$uc_gene/s && $row=~/$species/s) {
            #Grab data from columns
            #$row =~ m#.*?/gene/(.*?)>(.*?)</a>.*?#
            $is_found = 1;
            my $col_num = 1;
            
            while ($row=~m#<td.*?>(.*?)</td>#gs) {
                my $col = $1;
                #print "line $col_num: $col";
                #Get Symbol and name from col 1
                if ($col_num==1) {
                    #case 1: with span tags
                    if ($col =~ /link_uid=(.*?)"><$tag_c/s) {
                        ($sym, $entrez_id) = getTagValues($tag_c, $col);
                        $entrez_id =~/([0-9]+)/;
                        $entrez_id = $1;
                    #case 2: w/o span tags
                    }elsif($col =~ m#link_uid=(.*?)">(.*?)<#s){
                        $sym = trim($2);
                        $entrez_id = trim($1);
                        
                    }
                #Get full name from col 2
                }elsif($col_num==2){
                    #case 1: with span tags
                    if ($col =~ m#<$tag_c.*?>(.*?)</$tag_c>(.*?)\[#s) {
                        $fullname = $1 . $2;
                        #print "inner 1: $fullname";
                    #case 2: w/o span tags
                    }elsif($col =~m#(.*?)\[#s){
                        $fullname = trim($1);
                        #print "inner 2: $fullname";
                    }
                    
                    push @alias, $sym;
                    #print "Full Name: $fullname";
                #Get Aliases from col 4
                }elsif($col_num==4){
                    my @tag_val = getTagValues($tag_c, $col);
                    my @raw_alias = split/,/,$col;
                    foreach (@raw_alias){
                        #case 1: with span tag
                        if ($_=~/$tag_c/) {
                            push @alias, getTagValues($tag_c, $_);
                        #case 2: w/o span tag
                        }else{
                            push @alias, trim($_);
                        }
                    }
                #print "@alias\n";    
                }
                $col_num++;
            }           
        }       
    } 
}elsif($content =~ m#<meta name="ncbi_report".*?content="full_report".*?/>#s){
    print "full summary report still need to parse";
}else{
    print "no results found";
}

}


foreach my $t (@alias) {
    
    $t =~ s/,//g;
    
    #list of services
    my @services = ("STRING=http://string.uzh.ch/psicquic/webservices",
                    "BioGrid=http://tyersrest.tyerslab.com:8805/psicquic/webservices",
                    "IntAct=http://www.ebi.ac.uk/Tools/webservices/psicquic/intact/webservices",
                    "iRefIndex=http://psicquic.irefindex.org/psicquic/webservices",
                    #"BIND=http://webservice.baderlab.org:8480/psicquic-ws/webservices",
                    );

    for my $service (@services){
        
        #seperate service name from service url
        my @flds = split(/=/, $service);
        
        my ($serviceName, $serviceUrl) = @flds;
        
        #call subroutine readPsicquic
        readPsicquic $serviceName, $serviceUrl, $t, 400;

    }

}
