#! usr/bin/perl -l
use strict;
use warnings;

use DBI;
require "id.lib";

#open file with verbs
open my $fh, "<", "dataVerbs.txt" or die "Cannot open file";
my $firstline1 = <$fh>;
my @interactVerb = split /,/, $firstline1;

#open file for pluri known genes 
open my $fh2, "<", "pluriGenes.txt" or die "Cannot open file";
my $firstline2 = <$fh2>;
my @pluriGenes = split /,/, $firstline2;
#get rid of title
shift @pluriGenes;

#open file for diff known genes 
open my $fh3, "<", "cellDiffGenes.txt" or die "Cannot open file";
my $firstline3 = <$fh3>;
my @cellDiffGenes = split /,/, $firstline3;
#get rid of title
shift @cellDiffGenes;

#open file for specification known genes 
open my $fh4, "<", "neuralSpecGenes.txt" or die "Cannot open file";
my $firstline4 = <$fh4>;
my @neuralLineSpecGenes = split /,/, $firstline4;
#get rid of title
shift @neuralLineSpecGenes;

#open file for nouns 
open my $fh5, "<", "interactNouns.txt" or die "Cannot open file";
my $firstline1a = <$fh5>;
my @interactNouns = split /,/, $firstline1a;

#open file containing all known human genes to find novel interaction 
open my $fh6, "<", "genelist3.tsv" or die "cannot open file";
my @lines6 = <$fh6>;

#to get rid of white space before and after gene name 
foreach (@lines6){
    $_=~ s/^\s+|\s+$//;
}

#open abstract file 
open my $fh7, "<", "masterlist.csv" or die "cannot open csv";
my @lines7 = <$fh7>;
chomp @lines7;
my $lines7a = join ",", @lines7;
my @parser_input = split /,/, $lines7a;


#making regex for pluripotent genes
my $regexplurigene = "";
    foreach (@pluriGenes) {
        $regexplurigene .= $_ . "|";
    }
$regexplurigene .= " ae ";
#print $regexplurigene;


#regex for cell line diff genes
my $regexcelldiffgene = "";    
    foreach (@cellDiffGenes) {
        $regexcelldiffgene .= $_ . "|";
    }
$regexcelldiffgene .= " ae ";
#print $regexcelldiffgene; 


# regex for specify germ line genes 
my $regexlinespecgene = "";
    foreach (@neuralLineSpecGenes) {
        $regexlinespecgene .= $_ . "|";  
    }
$regexlinespecgene .= " ae ";

    
#making regex for interact verb
my $regexverb1;
    foreach (@interactVerb) {
        $regexverb1 .= $_ . "|";
#        #print "$_\n";
    }
$regexverb1 .= " ae ";

#making regex for interact noun
my $regexnoun = "";
    foreach (@interactNouns) {
        $regexnoun .= $_ . "|";
    }
$regexnoun .= " ae ";


#making regex for novel gene
my $regexnovelgene;
    foreach (@lines6) {
        $regexnovelgene .= '\b' . $_ . '\b|';
   }
$regexnovelgene .= " ae ";


my @old_verb_genelist;
my @verblist;
my @new_verb_genelist;

my $old_verb_gene;
my $verb;
my $new_verb_gene;

my @nounlist;
my @old_noun_genelist;
my @new_noun_genelist;

my $noun;
my $old_noun_gene;
my $new_noun_gene;

my @insert_verb_ppi;
my @insert_noun_ppi;

for (my $i=5; $i<scalar @parser_input; $i+=5){
    while($parser_input[$i] =~ /($regexplurigene|$regexcelldiffgene|$regexlinespecgene)(.*?)($regexverb1)(.*?)($regexnovelgene)/ig){
        $old_verb_gene = $1;
        $verb = $3;
        $new_verb_gene = $5;
        
        push @old_verb_genelist, $old_verb_gene;
        push @verblist, $verb;
        push @new_verb_genelist, $new_verb_gene;
        push @insert_verb_ppi, $parser_input[$i-1];
        push @insert_verb_ppi, $parser_input[$i-4];
        push @insert_verb_ppi, $parser_input[$i-5];        
        }
    
     while($parser_input[$i] =~ /($regexnovelgene)(.*?)($regexverb1)(.*?)($regexplurigene|$regexcelldiffgene|$regexlinespecgene)/ig){
        $old_verb_gene = $5;
        $verb = $3;
        $new_verb_gene = $1;
        
        push @old_verb_genelist, $old_verb_gene;
        push @verblist, $verb;
        push @new_verb_genelist, $new_verb_gene;
        push @insert_verb_ppi, $parser_input[$i-1];
        push @insert_verb_ppi, $parser_input[$i-4];
        push @insert_verb_ppi, $parser_input[$i-5];    
        }
     
    while($parser_input[$i] =~ /($regexnoun)(.*?)($regexnovelgene)(.*?)($regexplurigene|$regexcelldiffgene|$regexlinespecgene)/ig){
        $noun = $1;
        $new_noun_gene = $3;
        $old_noun_gene = $5;
        
        push @old_noun_genelist, $old_noun_gene;
        push @nounlist, $noun;
        push @new_noun_genelist, $new_noun_gene;
        push @insert_verb_ppi, $parser_input[$i-1];
        push @insert_verb_ppi, $parser_input[$i-4];
        push @insert_verb_ppi, $parser_input[$i-5];
        
        #print "there is a $noun with $old_noun_gene and $new_noun_gene \n";
        
        }
    
      
    while($parser_input[$i] =~ /($regexnoun)(.*?)($regexplurigene|$regexcelldiffgene|$regexlinespecgene)(.*?)($regexnovelgene)/ig){
        $noun = $1;
        $old_noun_gene = $3;
        $new_noun_gene = $5;
        
        push @old_noun_genelist, $old_noun_gene;
        push @nounlist, $noun;
        push @new_noun_genelist, $new_noun_gene;
        push @insert_verb_ppi, $parser_input[$i-1];
        push @insert_verb_ppi, $parser_input[$i-4];
        push @insert_verb_ppi, $parser_input[$i-5];    
        }
}

 
    # connect to database - fill in appropriate user details
    my $sql = "null";
    my $username = "null";
    my $host = "null";
    my $dbh =DBI->connect($host, $username,$sql) or die "cannot open";
    
    
    #inserting into parent protein table for verbs
    foreach (@new_verb_genelist){
        my ($id, $sym, $full_name, @alias) = getGeneInfo($_);
        my $query1 = "insert into protein (p_id, p_sym, p_fullname) values (?,?,?)";
        my $sth = $dbh->prepare($query1) or die "cannot prepare";
        my $z = $sth->execute($id, $sym, $full_name);
        push @insert_verb_ppi, $id;
        print " verb protein id: $id\n";
    }
    
    #insert into parent protein table for nouns
    foreach (@new_noun_genelist){
        my ($id, $sym, $full_name, @alias) = getGeneInfo($_);
        my $query1 = "insert into protein (p_id, p_sym, p_fullname) values (?,?,?)";
        my $sth = $dbh->prepare($query1) or die "cannot prepare";
        my $z1 = $sth->execute($id, $sym, $full_name);
        push @insert_noun_ppi, $id;
    }
    
    #getting entrez id for old protein found with a verb 
    foreach (@old_verb_genelist){
        my ($id, $sym, $full_name, @alias) = getGeneInfo($_);
        push @insert_verb_ppi, $id;
        print "verb old protein id : $id\n";
    }
    
    #gettting protein entrez id from those found in noun list 
     foreach (@old_noun_genelist){
        my ($id, $sym, $full_name, @alias) = getGeneInfo($_);
        push @insert_noun_ppi, $id;
    }
    
    #inserting into interaction type from noun list
    foreach (@nounlist){
        my $query2 = "insert into int_type values ('', ?)";
        my $sth = $dbh->prepare($query2) or die "cannot prepare";
        my $z2 = $sth->execute($_);
        
    }
    
    #inserting into interaction type from verb list 
    foreach (@verblist){
        my $query2 = "insert into int_type values ('', ?)";
        my $sth = $dbh->prepare($query2) or die "cannot prepare";
        my $z3 = $sth->execute($_);
        print "was adding into int table successufl: $z3";
    }
    
    #getting interaction id to insert into ppi table from noun list
    foreach (@nounlist){
        my $queryppi= "select int_type_id from int_type where int_type_name=?";
        (my $sth2, my $a) = run_query($dbh, $queryppi, [$_]);
        my @result = $sth2->fetchrow_array;
        push @insert_noun_ppi, $result[0];
        
    }
    
    #getting interaction id to insert into ppi table from verb list 
    foreach (@verblist){
        my $queryppi= "select int_type_id from int_type where int_type_name=?";
        my ($sth2, my $a) = run_query($dbh, $queryppi, [$_]);
        my @result = $sth2->fetchrow_array;
        push @insert_verb_ppi, $result[0];
        print "int id: $result[0]";
        
    }
    
    
    #insert into ppi table
    for (my$i=0; $i<=scalar @insert_verb_ppi; $i+=6){
        my $query1 = "insert into ppi (pA_id, pB_id, url, date_published, source_name, int_type_id) values (?,?,?,?,?,?)";
        my $sth = $dbh->prepare($query1) or die "cannot prepare";
        my $z4 = $sth->execute($insert_verb_ppi[$i+3], $insert_verb_ppi[$i+4], $insert_verb_ppi[$i], $insert_verb_ppi[$i+1],$insert_verb_ppi[$i+2],$insert_verb_ppi[$i+5]);
        print "the number of rows affected is $z4";
    }
    
    for(my$i=0; $i<=scalar @insert_noun_ppi; $i+=6){
        my $query1 = "insert into ppi (pA_id, pB_id, url, date_published, source_name, int_type_id) values (?,?,?,?,?,?)";
        my $sth = $dbh->prepare($query1) or die "cannot prepare";
        my $z5 = $sth->execute($insert_verb_ppi[$i+3], $insert_verb_ppi[$i+4], $insert_verb_ppi[$i], $insert_verb_ppi[$i+1],$insert_verb_ppi[$i+2],$insert_verb_ppi[$i+5]);
    }
    
 
    
    

    
     
